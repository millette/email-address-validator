/**
 * Email address validator module.
 *
 * @module email-address-validator
 * @author Robin Millette <robin@millette.info>
 * @copyright 2015 (Montréal, Québec, Canada)
 * @license AGPL-3.0
 * @requires publicsuffixlist
 * @requires lodash
 * @requires q
 *
 * @todo Make an actual packaged release for npm.
 */

'use strict';

// core
var dns = require('dns'),

  // npm
  PublicSuffixList = require('publicsuffixlist'),
  //debug = require('debug')('eav'),
  _ = require('lodash'),
  Q = require('q'),

  // inner variables
  psl = new PublicSuffixList(),

  /**
   * Initialize the public suffix list
   * .
   * @name init
   * @function
   * @inner
   * @private
   * @since 0.0.1
   */
  init = _.once(function () {
    // conveniently, lodash and Q play along just fine
    // first, transforming psl.initialize(callback) to return a promise
    // secondly, using lodash's _once() making sure
    //  it only gets called once
    var deferred = Q.defer();
    psl.initialize(function (error) {
      if (error) { deferred.reject(new Error(error)); }
      else { deferred.resolve(); }
    });
    return deferred.promise;
  }),

  /**
   * Return an error constructor.
   *
   * @access private
   * @param {string} name Name of the error.
   * @param {string} message Message of the error,
   *                 leave undefined to let the constructor
   *                 handle the message.
   * @return {Error} Return a typed error constructor
   *                 (optionnally taking a message argument).
   * @see module:email-address-validator.BadUsernameError
   * @see module:email-address-validator.MalformedEmailAddressError
   * @see module:email-address-validator.MxError
   * @see module:email-address-validator.NoExchangeError
   * @see module:email-address-validator.PSLError
   * @since 0.0.1
   *
   */
  errorFactory = function (name, message) {
    var o = ('string' === typeof message) ? function () {
      this.name = name;
      this.message = message;
    } : function (msg) {
      this.name = name;
      this.message = msg;
    };

    o.prototype = Error.prototype;
    return o;
  };

/** Email address validator functions. */
module.exports = {
  // exceptions
  /**
   * Malformed email address error. No @ separating 2 parts,
   * doesn't look like an email address.
   *
   * @class module:email-address-validator.MalformedEmailAddressError
   * @extends Error
   * @since 0.0.1
   */
  MalformedEmailAddressError: errorFactory(
    'MalformedEmailAddressError',
    'No @ separating 2 parts, doesn\'t look like an email address.'
  ),

  /**
   * Bad username error. Username part of the email address
   * doesn't pass our regexp.
   *
   * @class module:email-address-validator.BadUsernameError
   * @extends Error
   * @since 0.0.1
   */
  BadUsernameError: errorFactory(
    'BadUsernameError',
    'Username part of the email address doesn\'t pass our regexp.'
  ),

  /**
   * Public Suffix List error. Domain part of the email address
   * doesn't pass our Public Suffix List test.
   *
   * @class module:email-address-validator.PSLError
   * @extends Error
   * @since 0.0.1
   */
  PSLError: errorFactory(
    'PSLError',
    'Domain part of the email address doesn\'t pass our Public Suffix List test.'
  ),

  /**
   * MX error. No MX DNS entry found matching the specified email address.
   *
   * @class module:email-address-validator.MxError
   * @param {string} message Error message.
   * @extends Error
   * @since 0.0.1
   */
  MxError: errorFactory('MxError'),

  /**
   * No MX exchange error. No known exchange for this email address.
   *
   * @class module:email-address-validator.NoExchangeError
   * @extends Error
   * @since 0.0.1
   */
  NoExchangeError: errorFactory(
    'NoExchangeError', 'No known exchange for this email address.'
  ),

  // sync
  /**
   * Is email's username valid?
   *
   * @param {string} user The username part of an email address to test.
   * @return {boolean} True or false according to validity.
   * @since 0.0.1
   */
  username: function (user) {
    // half of https://github.com/StoneCypher/node-validate-email
    // the other half is handled by the public suffix list
    return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))$/i.test(user);
  },

  /**
   * After we split an email address in two parts, we get this object.
   *
   * @typedef {Object} EmailParts
   * @property {string} user - User part of the email address,
   *                           before the @.
   * @property {string} domain - Domain part of the email address,
   *                             after the @.
   * @since 0.0.1
   */

  /**
   * Split in user and domain parts the given email address on @.
   *
   * @param {string} email The email address to test.
   * @return {EmailParts|false} Object
   *                            {@link module:email-address-validator~EmailParts|EmailParts}
   *                            with user and domain properties
   *                            or false on a malformed string.
   * @since 0.0.1
   */
  getTwoParts: function (email) {
    var p;
    // a@b.ca is the shortest possible email address, hence 6
    if ('string' !== typeof email || email.length < 6) { return false; }
    p = email.split('@');
    // having a truthy p[2] would mean
    // there are at least 2 @ in the email
    return p[0] && p[1] && !p[2] ? {user: p[0], domain: p[1]} : false;
  },

  // promises
  /**
   * Is email valid according to local tests?
   *
   * @param {string} email The email address to test.
   * @see module:email-address-validator.username
   * @throws {module:email-address-validator.MalformedEmailAddressError}
   * @throws {module:email-address-validator.BadUsernameError}
   * @throws {module:email-address-validator.PSLError}
   * @since 0.0.1
   */
  local: function (email) {
    //local: _.memoize(function (email) {
    // I'm just along for the ride,
    // following examples when it comes to promises
    var p, deferred = Q.defer();

    // with exactly 2 parts separated by a @
    p = module.exports.getTwoParts(email);
    if (false === p) {
      deferred.reject(new module.exports.MalformedEmailAddressError());
      return deferred.promise;
    }

    // and test against some email regexp
    if (!module.exports.username(p.user)) {
      deferred.reject(new module.exports.BadUsernameError());
      return deferred.promise;
    }

    // now we can actually init
    // init() will only by called once thanks to lodash
    // then() will always follow,
    // unless we're missing the public suffix list data file
    return init().then(function () {
      // can we call validate already? gee, thanks!
      if (psl.validate(p.domain)) { deferred.resolve(); }
      else { deferred.reject(new module.exports.PSLError()); }
      return deferred.promise;
    });
  },

  /**
   * Is email valid according only to remote tests?
   *
   * @name remoteOnly
   * @function
   * @static
   * @param {string} email The email address to test.
   * @throws {module:email-address-validator.MalformedEmailAddressError}
   * @throws {module:email-address-validator.MxError}
   * @throws {module:email-address-validator.NoExchangeError}
   * @since 0.0.1
   */
  // Using lodash's memoize to cache results for given arguments.
  // This is also why we had to resort to "@name remoteOnly",
  // "@function" and "@static" in the JSDoc above.
  remoteOnly: _.memoize(function (email) {
    var p, deferred = Q.defer();

    // crude email address verification to grab what's after the @
    p = module.exports.getTwoParts(email);
    if (false === p) {
      deferred.reject(new module.exports.MalformedEmailAddressError());
      return deferred.promise;
    }

    dns.resolveMx(p.domain, function (error, mx) {
      if (error) { deferred.reject(new module.exports.MxError(error.message)); }
      else {
        // if the resolveMx response doesn't have an object
        // with an exchange key, we're not interested
        if (mx.length && 'string' === typeof mx[0].exchange) { deferred.resolve(); }
        else { deferred.reject(new module.exports.NoExchangeError()); }
      }
    });

    return deferred.promise;
  }),

  /**
   * Is email valid according to local and remote tests?
   *
   * @param {string} email The email address to test.
   * @throws {module:email-address-validator.MalformedEmailAddressError}
   * @throws {module:email-address-validator.MxError}
   * @throws {module:email-address-validator.NoExchangeError}
   * @throws {module:email-address-validator.BadUsernameError}
   * @throws {module:email-address-validator.PSLError}
   * @see module:email-address-validator.local
   * @see module:email-address-validator.remoteOnly
   * @since 0.0.1
   */
  remote: function (email) {
    var deferred = Q.defer();
    // start with local() test since there's no point otherwise
    module.exports.local(email).then(function () {
      module.exports.remoteOnly(email).then(function () {
        deferred.resolve();
      }).catch(function (error) {
        deferred.reject(error);
      });
    }).catch(function (error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }
};
