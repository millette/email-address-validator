# Email address validator

Validate email addresses locally with a regexp and
using the Public Suffix List and remotely by looking up
the DNS MX field for a given email address.

Homepage: [code.librizine.org][]<br>
Git: [gitlab][]

## Install

**Note that there is currently no released package.**

```bash
$ npm install --save email-address-validator
```

## Usage

```javascript
var emailAddressValidator = require('email-address-validator');
// ...
```

## API

See the [generated JSDocs][] for the API documentation.

## Contributing

In lieu of a formal styleguide, take care to maintain the existing
coding style. Add unit tests for any new or changed functionality.
Lint and test your code using [gulp][].

See you on **IRC** in #librizine on the Freenode network.

### Various development commands
```bash
$ gulp          # run unit tests and report code coverage
$ gulp lint     # test format, syntax with jscs and jshint
$ gulp istanbul # code coverage report
$ gulp watch    # run tests then watch file changes and act accordingly
$ gulp bump     # increase package.json's version patch by one in
$ npm run jsdoc # generate api docs
```

## License

Copyright 2015 [Robin Millette][]. Licensed under the AGPL-3.0 license.

[generated JSDocs]: module-email-address-validator.html
[Robin Millette]: http://robin.millette.info/
[gulp]: http://gulpjs.com/
[code.librizine.org]: http://code.librizine.org/email-address-validator/0.0.2/
[gitlab]: https://gitlab.com/millette/email-address-validator
