# How did we get here?

```bash
$ mkdir myproject
$ cd myproject
$ yo node-gulp # yes to everything (but Coveralls)
$ npm install jsdoc --save-dev
$ npm install chai-as-promised --save-dev
$ npm uninstall should --save-dev # use chai and chai-as-promised instead
$ npm install nodemailer --save
$ npm install ../email-address-validator --save # currently only available locally
$ cp ../email-address-validator/jsdoc.json .
$ editor jsdoc.json # replace ./lib/email-address-validator.js with ./lib/woofwoof.js
$ # Add "jsdoc": "rm -fr docs && ./node_modules/jsdoc/jsdoc.js -c jsdoc.json" (with quotes)
$ # to the scripts object field.
$ editor package.json
```

We replaced ```should``` with ```chai``` and ```chai-as-promised```
as development dependencies, to let us test promises cleanly with
the "eventually" field. We must also edit **test/woofwoof_test.js**
to use this code:

```javascript
var woofwoof = require('../'),
  chai = require('chai'),
  chaiAsPromised = require('chai-as-promised');

chai.should();
chai.use(chaiAsPromised);
```

These two lines should be replaced:

```javascript
var woofwoof = require('../');
var assert = require('should');
```

At this point, everything should be working as expected.
