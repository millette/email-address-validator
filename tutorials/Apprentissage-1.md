# Leçons sur la création d'un (tout petit) module NodeJS - partie 1

## Intro

Bla bla bla.

Le packaging du module se trouve dans la 2e partie.

## Les affaires

### Public Suffix List (PSL)
Je ne peux compter le nombre de fois où mon adresse email a été refusée tout simplement parce qu'elle se termine par ".info" au lieu des
".com", ".ca" et ".org" plus communs. Pourtant, mon adresse est bien valide, je l'utilise depuis plus de 10 ans.

Quand il s'agit d'une validation côté client, j'essaie souvent de déterminer comment l'erreur s'est produite pour envoyer un email complet aux responsables du site. Très souvent, cette validation va supposer que les domaines se terminent par 2 ou 3 caractères après le dernier point, ni plus, ni moins. Mon ".info" s'y plante, ainsi que tous les nouveaux utilisateurs du ".quebec" par exemple, puisque 6 lettres, c'est beaucoup trop demander!

Et encore, chanceux que je n'utilise pas quelque chose comme robin@millette.example.com - avec trois parties séparées par des points après le @. Parfois c'est permis, parfois c'est obligatoire, variant selon les régions; honnêtement, difficile de s'y retrouver.

La Fondation Mozilla a eu la brillante idée de colliger une [liste des suffixes publics][psl] (PSL). C'est disponble dans un fichier de plus de 10,000 lignes qui pèse environ 150 KiB. Il existe des librairies pour tous les langages populaires (où un langage est dit populaire quand il existe une implémentation de PSL pour), je ne me suis attardé qu'à une des implémentations pour JavaScript et qui fonctionnait bien dans NodeJS.

J'avais maintenant entre les mains une liste maintenue et plus d'excuse telle qu'une expression régulière incomplète pour refuser une adresse email valide. Du moins, j'avais la moitié de ma solution, soit la validation du domaine.

L'autre moitié, la validation du nom d'utilisateur (la partie qui précède le @) est venue de https://github.com/StoneCypher/node-validate-email et ensemble, j'ai pu obtenir une solution très fiable.

Ça constitue l'aspect **local** du test d'adresse email. L'aspect final, **remote**, consiste à découvrir s'il existe un champ DNS MX correspondant à l'adresse email. Je dis final, mais on ne pourra **confirmer** qu'au moment d'envoyer un message (si la boite est pleine, etc.).

### JSDoc
Ah, la documentation!

### istanbul
Après les tests, on voit d'un coup d'oeil quel pourcentage du code on a testé.

### Lodash
J'avais déjà utilisé lodash avant. C'est ce que j'utilise par défaut pour des templates, par exemple.

Ici, j'ai utilisé ```once``` et ```memoize```

#### Rencontre avec JSDoc
Au lien d'avoir une fonction comme

```javascript
/**
 * Bla is bob.
 *
 * @param {boolean} thing Good (true) or bad (false).
 */
var bob = function (thing) {
};
```

Comme la fonction prend un certain temps à rouler, parce que c'est un appel réseau par exemple, on passe par memoize de lodash pour capturer les résultats de la fonction pour chaque thing passé.

```javascript
var bob = _.memoize(function (thing) {
});
```
C'est là que JSDoc ne reconnait plus la fonction. Il faut donc utiliser ce bloc de doc:


```javascript
/**
 * Bla is bob.
 *
 * @name bob
 " @type function
 * @param {boolean} thing Good (true) or bad (false).
 */
```

Et la doc devrait maintenant être générée pour désiré.

### Mochai et Chai
Le *test driven development*, vous connaissez? C'est une pratique qui vient du monde Agile (on disait Extrème bien avant) où les codes unitaires viennent avant d'écrire le code lui-même. On commence avec les tests avant d'avoir implémenter quoi que ce soit d'autre. Vous ne serez pas surpris d'apprendre que ces tests vont échouer - ou alors vous avez déjà un problème avec vos tests!

Écrire les tests en premier va discipliner notre esprit et notre code devrait en être plus robuste puisqu'il a d'abord été écrit pour être utilisé.

Mochai est un *test runner*, tandis que Chai est une librairie d'assertions. Dans mon cas, Mochai utilise Chai, mais on va un peu plus loin à cause de l'utilisation des Promises avec Q.

### Q
Des promesse, toujours des promesses.

#### Rencontre avec Lodash
once() de Lodash est utilisé pour s'assurer que la liste PSL n'est initialisée qu'une seule fois. L'initialisation retourne une Promise et cette dernière est gentiement acceptée par once() et passe le flambeau à la prochaine fonction dans la course.

### Chai-as-promised
Au lieu du bon vient bla().should.be.equal(42), on va utiliser bla().should.eventually.be.equal(42) quand 42 vient d'une promesse. Sans chai-as-promised, on doit utiliser le callback done() et ça rend le code un peu plus compliqué. Surtout que ce n'est pas rare d'avoir de 25 à 250 tests unitaires.

### gulp
Avant de commencer un projet, je me tourne vers yeoman pour voir s'il existe un générateur. Quand j'ai l'option entre grunt et gulp comme "task runner", je préfère gulp. Bref, j'ai lancé un

```bash
$ mkdir mon-proj
$ cd mon-proj
$ yo node-gulp
```

Et je me suis retrouvé avec une belle petite config gulp, q, lodash, jscs et autres outils pratiques.

```bash
$ gulp          # run unit tests and report code coverage
$ gulp lint     # test format, syntax with jscs and jshint
$ gulp watch    # run tests then watch file changes and act accordingly
```


### npm

```bash
$ npm run jsdoc # generate api docs
```

### git

### jscs

## Conclu

[psl]: https://publicsuffix.org/
