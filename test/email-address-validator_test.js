/**
 * @file Unit tests.
 * @author Robin Millette <robin@millette.info>
 */

'use strict';

var verifyEmail = require('../'),
  chai = require('chai'),
  chaiAsPromised = require('chai-as-promised');

chai.should();
chai.use(chaiAsPromised);

describe('username', function () {
  it('robin should be true', function () {
    return verifyEmail.username('robin').should.equal(true);
  });

  it('robin.millette should be true', function () {
    return verifyEmail.username('robin.millette').should.equal(true);
  });

  it('a@ should be false', function () {
    return verifyEmail.username('a@').should.equal(false);
  });

  it('a. should be false', function () {
    return verifyEmail.username('a.').should.equal(false);
  });
});

describe('local (async)', function () {
  it('robin@millette.info should succeed', function () {
    return verifyEmail.local('robin@millette.info').should.eventually.be.undefined;
  });

  it('robin666@millette.info should succeed', function () {
    return verifyEmail.local('robin666@millette.info').should.eventually.be.undefined;
  });

  it('robin@millette666.info should succeed', function () {
    return verifyEmail.local('robin@millette666.info').should.eventually.be.undefined;
  });

  it('robin@millette.info666 should be rejected', function () {
    return verifyEmail.local('robin@millette.info666').should.eventually.be.rejectedWith(verifyEmail.PSLError);
  });

  it('a@b.ca should succeed', function () {
    return verifyEmail.local('a@b.ca').should.eventually.be.undefined;
  });

  it('.robin@example.com should be rejected', function () {
    return verifyEmail.local('.robin@example.com').should.eventually.be.rejectedWith(verifyEmail.BadUsernameError);
  });

  it('robin@ should be rejected', function () {
    return verifyEmail.local('robin@').should.eventually.be.rejectedWith(verifyEmail.MalformedEmailAddressError);
  });

  it('a@b.ce should be rejected', function () {
    return verifyEmail.local('a@b.ce').should.eventually.be.rejectedWith(verifyEmail.PSLError);
  });

  it('niet should be rejected', function () {
    return verifyEmail.local('niet').should.eventually.be.rejectedWith(verifyEmail.MalformedEmailAddressError);
  });
});

describe('remoteOnly (async)', function () {
  it('niet should be rejected', function () {
    return verifyEmail.remoteOnly('niet').should.eventually.be.rejectedWith(verifyEmail.MalformedEmailAddressError);
  });

  it('robin@millette.info should succeed', function () {
    return verifyEmail.remoteOnly('robin@millette.info').should.eventually.be.undefined;
  });

  it('robin666@millette.info should succeed', function () {
    return verifyEmail.remoteOnly('robin666@millette.info').should.eventually.be.undefined;
  });

  it('robin@millette666.info should be rejected', function () {
    return verifyEmail.remoteOnly('robin@millette666.info').should.eventually.be.rejectedWith(verifyEmail.MxError);
  });

  it('robin@millette.info666 should be rejected', function () {
    return verifyEmail.remoteOnly('robin@millette.info666').should.eventually.be.rejectedWith(verifyEmail.MxError);
  });
});

describe('remote (async)', function () {
  it('robin@millette.info should succeed', function () {
    return verifyEmail.remote('robin@millette.info').should.eventually.be.undefined;
  });

  it('robin666@millette.info should succeed', function () {
    return verifyEmail.remote('robin@millette.info').should.eventually.be.undefined;
  });

  it('robin@millette666.info should be rejected', function () {
    return verifyEmail.remote('robin@millette666.info').should.eventually.be.rejectedWith(verifyEmail.MxError);
  });

  it('robin@millette.info666 should be rejected', function () {
    return verifyEmail.remote('robin@millette.info666').should.eventually.be.rejectedWith(verifyEmail.PSLError);
  });

  it('robin@example.com should be rejected', function () {
    return verifyEmail.remote('robin@example.com').should.eventually.be.rejectedWith(verifyEmail.MxError);
  });

  it('a@b.ca should be rejected', function () {
    return verifyEmail.remote('a@b.ca').should.eventually.be.rejectedWith(verifyEmail.MxError);
  });

  it('niet should be rejected', function () {
    return verifyEmail.remote('niet').should.eventually.be.rejectedWith(verifyEmail.MalformedEmailAddressError);
  });
});
